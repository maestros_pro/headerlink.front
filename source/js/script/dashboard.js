import FormFields from '../modules/mod.form-fields'
import Styck from '../modules/module.styck-nav'
import mPopupCategory from '../modules/mod.category-popup'
import checkbox from '../modules/mod.checked-callback'
import hint from '../modules/module.hint'
import plugin from '../modules/mod.plugins-load'
import Popup from '../modules/module.popup'
import Tab from '../modules/module.tab'
import loadAvatar from '../modules/mod.load-avatar'

let styck = new Styck({element: '.dashboard__aside'});

window.app = window.app || {};


$(function () {

	let tab = new Tab({
		classActiveLink: 'is-active',
		classActiveTab: 'is-active',
		onTabChange: function () {
		}
	});


	$('.gallery__list').mCustomScrollbar({
		axis:"x",
		advanced:{autoExpandHorizontalScroll:true}
	});

	FormFields.init();

	styck.init();

	let popup = new Popup({
		bodyClass: 'is-blur',
		onPopupOpen:()=>{
			plugin.init();
		}
	});

	window.app.popup = popup;
	window.app.formCheck = FormFields.check();
	window.app.pluginUpdate = ()=>plugin.init();

	plugin.init();

	let $b = $('body');


	$b
		.on('click', '.aside__link_group', function () {
			let $t = $(this),
				$group = $t.closest('.aside__group'),
				$menu = $group.find('.aside__menu')
			;

			if ( !$t.hasClass('is-busy') && $menu.length ){
				$t.addClass('is-busy');

				$t.toggleClass('is-active');
				$group.toggleClass('is-active');
				$menu.slideToggle(300, ()=>{
					$group.toggleClass('is-open');
					$t.removeClass('is-busy');
					styck.setPosition();
				})
			}


		})

		.on('click', '[data-aside-more]', function (e) {
			e.preventDefault();
			let $t = $(this),
				$aside = $($t.attr('data-aside-more')),
				$overlay = $('.aside__overlay')
			;

			if ( !$t.hasClass('is-busy') ){
				$t.addClass('is-busy');

				$('[data-aside-more]').not($t).removeClass('is-active');
				$('.aside__more').not($aside).removeClass('is-active');
				$t.toggleClass('is-active');
				$aside.toggleClass('is-active');

				setTimeout(()=>{
					if ( $('.aside__more').filter('.is-active').length ) {
						$overlay.fadeIn(250, ()=>{$t.removeClass('is-busy')});
					} else {
						$overlay.fadeOut(250, ()=>{$t.removeClass('is-busy')});
					}
				}, 10);
			}

		})

		.on('click', '.js-dropmenu-open', function (e) {
			e.preventDefault();
			let $t = $(this),
				$wrap = $t.closest('.js-dropmenu')
			;

			$('.js-dropmenu').not($wrap).removeClass('is-open');

			$wrap.toggleClass('is-open');

		})

		.on('click', '.js-dropmenu-close', function (e) {
			e.preventDefault();
			let $t = $(this),
				$wrap = $t.closest('.js-dropmenu')
			;

			$wrap.removeClass('is-open');

		})

		.on('change', '.section__table input[type=checkbox]', function (e) {
			let $t = $(this),
				$wrap = $t.closest('[data-tab-target]').length ? $t.closest('[data-tab-target]') : $t.closest('section'),
				$count = $wrap.find('.js-tablerow-chacked'),
				$checkAll = $wrap.find('.section__table-head').find('input[type=checkbox]'),
				$checkbox = $wrap.find('.section__table-row').find('input[type=checkbox]'),
				$checked = $checkbox.filter(':checked')

			;


			if ( $t.closest('.section__table-head').length ){
				if ( $t.prop('checked') ){
					$checkbox.prop('checked', true);
				} else {
					$checkbox.prop('checked', false);
				}
			} else {
				if( $checked.length === $checkbox.length ){
					$checkAll.prop('checked', true);
				} else {
					$checkAll.prop('checked', false);
				}
			}

			$checked = $checkbox.filter(':checked');

			setTimeout(()=>{$count.html($checked.length + '/' + $checkbox.length);},0)
		})
	;


	mPopupCategory.init();
	checkbox.init();
	hint.init();
	loadAvatar.init();
});