import FormFields from '../modules/mod.form-fields'
import mPopupCategory from '../modules/mod.category-popup'
import checkbox from '../modules/mod.checked-callback'
import hint from '../modules/module.hint'
import plugin from '../modules/mod.plugins-load'
import Popup from '../modules/module.popup'
import loadAvatar from '../modules/mod.load-avatar'


window.app = window.app || {};

$(function () {
	$('.cup_blur').removeClass('cup_blur');
});



$(function () {

	FormFields.init();

	let popup = new Popup({
		bodyClass: 'is-blur',
		onPopupOpen:()=>{
			plugin.init();
		}
	});
	plugin.init();


	window.app.popup = popup;
	window.app.formCheck = FormFields.check;

	let $b = $('body');

	$b

		.on('click', '[data-auth]', function (e) {
			e.preventDefault();
			let $t = $(this),
				val = $t.attr('data-auth');
			authMenu(val);
		})

		.on('click', '.js-auth', function (e) {
			e.preventDefault();
			authMenu('login');
		})
		.on('click', '.auth__close', function (e) {
			e.preventDefault();
			authMenu('hide');
		})





	;



	function authMenu(val) {
		let $auth = $('.auth'),
			$authOverlay = $auth.find('.auth__overlay'),
			$authLayout = $auth.find('.auth__layout'),
			isBuzy = false;

		if ( isBuzy ) return false;
		isBuzy = true;

		if ( val === 'hide' ){


			$authOverlay.fadeOut(600, ()=>{
				$authOverlay.removeAttr('style');
				$b.removeClass('is-auth');
				$auth.removeAttr('style');
				$authLayout.removeAttr('style');
				isBuzy = false;
			});
			$authLayout.animate({
				marginLeft: '-100%'
			}, 500, ()=>{
			});

		} else {

			if ( val ){
				$auth.find('.auth__content').removeClass('is-active').filter('.auth__content_' + val).addClass('is-active')
			}

			$auth.show();
			$authOverlay.fadeIn(600, ()=>{
				$authOverlay.removeAttr('style');
				$b.addClass('is-auth');
				$auth.removeAttr('style');
				$authLayout.removeAttr('style');
				isBuzy = false;
			});
			$authLayout.animate({
				marginLeft: 0
			}, 500, ()=>{

			});

		}
	}






	mPopupCategory.init();
	checkbox.init();
	hint.init();
	loadAvatar.init();








});


