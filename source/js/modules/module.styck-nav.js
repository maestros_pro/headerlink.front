'use strict';

export default class Styck {
	constructor(options){
		//this._polyfill();
		this.data = {};
		Object.assign(this._options = {}, this._default(), options);
	}

	_polyfill() {

		if (typeof Object.assign !== 'function') {
			Object.assign = function (target, varArgs) { // .length of function is 2
				'use strict';
				if (target === null) { // TypeError if undefined or null
					throw new TypeError('Cannot convert undefined or null to object');
				}

				var to = Object(target);

				for (var index = 1; index < arguments.length; index++) {
					var nextSource = arguments[index];

					if (nextSource !== null) { // Skip over if undefined or null
						for (var nextKey in nextSource) {
							// Avoid bugs when hasOwnProperty is shadowed
							if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
				}
				return to;
			};
		}

	}

	_default(){
		return {
			element: null
		}
	}

	_getCoords(el) {
		let box = el.getBoundingClientRect();

		return {
			top: box.top + pageYOffset,
			left: box.left + pageXOffset
		};
	}

	_setStyle(obj){
		Object.keys(obj).map((key)=>{
			if ( this.data.element.style[key] !== obj[key] ) this.data.element.style[key] = obj[key];
		});

	}

	setPosition(){
		let data = {};

		data.toBottom = (window.pageYOffset - this.data.scrollTop) >= 0;
		data.scrolled = window.pageYOffset || document.documentElement.scrollTop;
		data.elHeight = this.data.element.offsetHeight;
		data.elTop = this._getCoords(this.data.element).top;
		data.winHeight = window.innerHeight;
		data.parentTop = this._getCoords(this.data.parent).top;
		data.parentHeight = this.data.parent.offsetHeight;


		if ( data.scrolled < data.parentTop ){
			//if (this.data.parent.style.minHeight) this.data.parent.style.minHeight = '';
			this._setStyle({
				position: '',
				top: ''
			});
		} else if ( data.scrolled + data.winHeight > data.parentHeight + data.parentTop ){
			if (this.data.parent.style.minHeight) this.data.parent.style.minHeight = '';
			this._setStyle({
				position: 'absolute',
				top: data.parentHeight - data.elHeight + 'px'
			});
		} else {
			if ( data.elHeight <= data.winHeight ){
				if (this.data.parent.style.minHeight) this.data.parent.style.minHeight = '';
				this._setStyle({
					position: 'fixed',
					top: 0
				});
			} else {
				this.data.parent.style.minHeight = data.elHeight + 'px';
				if ( data.toBottom && data.scrolled + data.winHeight >= data.elHeight + data.elTop) {
					this._setStyle({
						position: 'fixed',
						top: '',
						bottom: 0
					});
				} else if ( !data.toBottom && data.scrolled <= data.elTop ){
					this._setStyle({
						position: 'fixed',
						top: 0,
						bottom: ''
					});
				} else {
					this._setStyle({
						position: 'absolute',
						top: data.elTop - data.parentTop + 'px',
						bottom: ''
					});
				}

			}
		}

	}

	init(){

		this.data.element = document.querySelector(this._options.element);
		this.data.parent = this.data.element.parentNode;
		this.data.scrollTop = window.pageYOffset;

		window.onscroll = ()=>{
			this.setPosition();
			this.data.scrollTop = window.pageYOffset;
		};
		window.onresize = ()=>this.setPosition();
		this.setPosition();
	}


}