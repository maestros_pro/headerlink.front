
export default class Template{

	constructor(options) {

		this._polyfill();

		Object.assign(this._options = {}, this._default(), options);
		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this._init();
			});
		} else {
			this._init();
		}
	}

	_default() {
		return {
			onCloned: function () {}
		}
	}

	_polyfill(){
		Element.prototype.contentTemplate = function(){

			return function( element ){

				if(element.content){
					return element.content;
				}

				let elPlate= element,
					qContent,
					docContent;

				qContent = elPlate.childNodes;
				docContent = document.createDocumentFragment();

				while(qContent[0]) {
					docContent.appendChild(qContent[0]);
				}

				elPlate.content = docContent;
				return  docContent;

			}(this);

		}
	}

	clone(el){
		let template = document.querySelector(el),
			wrap = template.parentNode,
			clone = template.content.cloneNode(true)
		;
		wrap.insertBefore(clone, template);

		if (this._options.onCloned && typeof this._options.onCloned === 'function') this._options.onCloned(clone);
		//return
	}

	_init(){

		document.body.addEventListener('click', (e) => {
			if (e.target.getAttribute('data-copy-template')) {
				e.preventDefault();
				let id = e.target.getAttribute('data-copy-template');
				this.clone(id);
			}
		});
	}
}