
export default {

	timer: null,

	_showHint(target){

		let $hint = $('.hint-bubble');

		if ( $hint.length < 1 ){
			$hint = $('<div/>', {
				class: 'hint-bubble'
			}).appendTo('body');
		}

		$hint.html($(target).attr('data-hint'))
			.show()
			.css({
				top: $(target).offset().top + $(target).outerHeight(),
				left: $(target).offset().left + ($(target).outerWidth()/2) - ($hint.outerWidth()/2)
			});
	},


	_hideHint(){
		$('.hint-bubble').hide();
	},

	init(){

		let $b = $('body');

		$b
			.on('mouseover', '[data-hint]', e=>{
				this.timer = setTimeout(()=>{
					this._showHint(e.target);
				}, 100);
			})
			.on('mouseleave', '[data-hint]', ()=>{
				clearTimeout(this.timer);
				this._hideHint();
			})
		;
	}
}