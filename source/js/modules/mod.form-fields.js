import Schema from '../modules/module.validate'
import Tooltip from '../modules/module.tooltip'
import Template from '../modules/module.template'
import plugin from '../modules/mod.plugins-load'
import Clipboard from 'clipboard'

let clipboard = new Clipboard('[data-clipboard-text]');
let sheme = new Schema();

export default {

	check(form, scroll, resolve, reject){

		let $form = $(form),
			$input = $form.find('[data-require]'),
			arr = [],
			$error = null,
			stop = false;

		if ( $input.length ){
			$input.each(function () {
				let $t = $(this),
					$field = $t.closest('.form__field'),
					$message = $field.find('.form__message');
				$message.html('');

				$t.closest('.form__field').removeClass('f-message f-error');
				sheme.validate($t[0], $t.attr('data-require'), r =>{
					if (r.errors.length) {
						if ( !$error ) $error = $t.closest('.form__field');
						arr.push({el:$t[0],name: $t.attr('name'), error: r.errors[0]});
						$message.html(r.errors[0]);
						stop = true;
						$t.closest('.form__field').addClass('f-message f-error');
					}
				})
			});
		}

		if ( stop ) {
			if (stop && scroll){
				let $scroller = $error.closest('.popup').length ? $error.closest('.popup__inner') : $('html, body'),
					scrollTop = $error.closest('.popup').length ? $error.closest('.popup__inner').scrollTop() + $error.eq(0).offset().top : $error.eq(0).offset().top;

				$scroller.animate({
					scrollTop: scrollTop
				}, 600);
			}
			if ( reject && typeof reject === 'function') reject(arr);
		}
		else {
			if ( resolve && typeof resolve === 'function') resolve();
		}
	},


	init(){

		jQuery.expr[':'].regex = function(elem, index, match) {
			let matchParams = match[3].split(','),
				validLabels = /^(data|css):/,
				attr = {
					method: matchParams[0].match(validLabels) ?
						matchParams[0].split(':')[0] : 'attr',
					property: matchParams.shift().replace(validLabels,'')
				},
				regexFlags = 'ig',
				regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
			return regex.test(jQuery(elem)[attr.method](attr.property));
		};

		let that = this;

		let tooltip = new Tooltip();

		let template = new Template({
			onCloned: (e)=>{
				plugin.init();
			}
		});

		let $b = $('body'),
			tooltipContent = {
				passCheckHold: false,
				passCheckInfo: {
					html: '<p>Пароль должен быть комбинацией из 6 ~ 15 букв (прописные / строчные), цифр и символов без пробелов.</p><p><br><a class="js-how-get-pass" href="#">Как подобрать пароль?</a></p>',
					direction: 'right'
				},
				passCheckFalse: {
					html: '<h2>Уровень надежности</h2><table><tr><td class="q-nw q-1_3"><span class="c-red">Непригодный</span></td><td>Пароль не отвечает требованиям</td></tr></table><p><br><a class="js-how-get-pass" href="#">Как подобрать пароль?</a></p>',
					direction: 'right'
				},
				passCheckWeak: {
					html: '<h2>Уровень надежности</h2><table><tr><td class="q-nw q-1_3"><span class="dots"><i class="c-red"></i><i></i><i></i></span><span class="c-red">Слабый</span></td><td>Пароль не достаточно безопасный</td></tr></table><p><br><a class="js-how-get-pass" href="#">Как подобрать пароль?</a></p>',
					direction: 'right'
				},
				passCheckModarete: {
					html: '<h2>Уровень надежности</h2><table><tr><td class="q-nw q-1_3"><span class="dots"><i class="c-yellow"></i><i class="c-yellow"></i><i></i></span><span class="c-yellow">Хороший</span></td><td>Пароль достаточно безопасный</td></tr></table><p><br><a class="js-how-get-pass" href="#">Как подобрать пароль?</a></p>',
					direction: 'right'
				},
				passCheckStrong: {
					html: '<h2>Уровень надежности</h2><table><tr><td class="q-nw q-1_3"><span class="dots"><i class="c-green"></i><i class="c-green"></i><i class="c-green"></i></span><span class="c-green">Отличный</span></td><td>Пароль очень безопасный</td></tr></table><p><br><a class="js-how-get-pass" href="#">Как подобрать пароль?</a></p>',
					direction: 'right'
				},
				passCheckManual: {
					close: true,
					title: 'Как выбрать пароль?',
					subtitle: '<div class="tooltip__subtitle">Вы можете подобрать надежный пароль <br>следуя нашим рекомендациям<br></div>',
					html: '	<h2>Уровень надежности</h2>'
					+'	<table>'
					+'		<tr>'
					+'			<td class="q-nw q-1_3"><span class="c-red">Непригодный</span></td>'
					+'			<td>Пароль не отвечает требованиям</td>'
					+'		</tr>'
					+'		<tr>'
					+'			<td class="q-nw q-1_3"><span class="dots"><i class="c-red"></i><i></i><i></i></span><span class="c-red">Слабый</span></td>'
					+'			<td>Пароль не достаточно безопасный</td>'
					+'		</tr>'
					+'		<tr>'
					+'			<td class="q-nw q-1_3"><span class="dots"><i class="c-yellow"></i><i class="c-yellow"></i><i></i></span><span class="c-yellow">Хороший</span></td>'
					+'			<td>Пароль достаточно безопасный</td>'
					+'		</tr>'
					+'		<tr>'
					+'			<td class="q-nw q-1_3"><span class="dots"><i class="c-green"></i><i class="c-green"></i><i class="c-green"></i></span><span class="c-green">Отличный</span></td>'
					+'			<td>Пароль очень безопасный</td>'
					+'		</tr>'
					+'	</table>'
					+'	<hr>'
					+'	<h2>Наши рекомендации</h2>'
					+'	<ul>'
					+'		<li>Пароль должен быть комбинацией из 6 ~ 15 букв (прописные / строчные), цифр и символов без пробелов.</li>'
					+'		<li>Используемые символы <br><br><span class="c-blue">! ? $ % & ( ) * + , - . / : ;  < > = ? @ # [ \\ ] ^ _  { | } ~</span></li>'
					+'		<li>В пароле не может использоваться пароль.</li>'
					+'		<li>Не используйте комбинацию из 4 и более повторяющихся букв и цифр.</li>'
					+'		<li>Не используйте в качестве пароля ваш номер телефона ли e-mail.</li>'
					+'	</ul>'
					+'</div>',
					direction: 'right'
				},
				regConf: {
					html: '<h2>Политика конфидециальности</h2>'
					+'		<p><br><a class="c-red" href="#">Ознакомьтесь с соглашением</a></p>',
					direction: 'right'
				}

			},
			phoneTemplateCounter = 0;

		$b
			.on('click', '.phone-reg-template-wrap .form__field-copy-add', function () {
				let $t = $(this),
					$wrap = $t.closest('.phone-reg-template-wrap'),
					$item = $wrap.find('.phone-reg-template-item'),
					full = true
				;
				$item.each(function () {
					let $inpt = $(this).find('input[type=tel]');

					if ( !$inpt.val() ){
						full = false;
						$inpt.focus();
					}
				});
				if ( !full ) return false;
				template.clone('#phoneNumberTemplate');

				$(":regex(name, .*{{.*)").each(function () {
					let $t = $(this),
						name = $t.attr('name'),
						val = parseInt(name.match(/\{\{(.+)\}\}/)[0].replace( /{{|}}/g, "" )),
						newName = name.replace( /{{(.+)}}/g, val+phoneTemplateCounter)
					;

					$t.attr('name', newName);

				});
				phoneTemplateCounter++;

			})

			.on('click', '.phone-reg-template-wrap .form__field-copy-remove', function () {
				let $t = $(this),
					$wrap = $t.closest('.phone-reg-template-wrap'),
					$item = $wrap.find('.phone-reg-template-item')
				;

				if ( $item.length > 1){
					$t.closest('.phone-reg-template-item').remove();
				} else {
					$t.closest('.phone-reg-template-item').find('input[type=tel]').val('').focus();
				}

				$item.find('input[type=tel]').trigger('change');

			})

			.on('change', '.phone-reg-template-wrap input[type=tel]', function () {
				let $t = $(this),
					$wrap = $t.closest('.phone-reg-template-item')
				;

				setTimeout(()=>{
					if (!$t.val()){
						$wrap.removeClass('_allow-copy');
					} else {
						$wrap.addClass('_allow-copy');
					}
				}, 0);

			})

			.on('change', '.form__input_logo', function (e) {
				let $t = $(this),
					$wrap = $t.closest('.form__input_logo'),
					$frame = $wrap.find('.image'),
					file = e.target.files[0]
				;

				if ( this.value && !/\.(jpg|jpeg|doc|docx|xls|xlsx|pdf)$/i.test( this.value ) ) {
					alert( 'Вы выбрали не верный тип файла \nДопустимые типы файлов: jpg, jpeg, doc, docx, xls, xlsx, pdf' );
				} else if (file) {
					let reader = new FileReader();

					reader.onloadend = function () {
						preview.src = reader.result;
					};

					reader.readAsDataURL(file);

					reader.onloadend = function() {
						$frame.css({
							backgroundImage: 'url(' + reader.result + ')'
						})
					};

				}

			})

			.on('click', '.form__input-unpass', function (e) {
				let $t = $(this),
					$input = $t.closest('.form__input').find('input');

				if ( $input.attr('type') === 'password' ){
					$input.attr('type', 'text');
				} else {
					$input.attr('type', 'password');
				}

			})

			.on('click', '.js-form-change', function (e) {
				e.preventDefault();
				let $t = $(this),
					$form = $t.closest('form');
				$form.addClass('f-changed').find('[disabled]').prop('disabled', false);
			})

			.on('click', '.js-form-unchange', function () {
				let $t = $(this),
					$form = $t.closest('form');
				$form.removeClass('f-changed');
				$form.find('[data-change=disabled]').prop('disabled', true);
			})

			.on('change', '.form input, .form select', function () {
				let $t = $(this),
					$form = $t.closest('form');

				if ( $form.find('.f-changin').length ) $form.addClass('f-changed').find('[disabled]').prop('disabled', false);

			})

			.on('focus', '.form__input input, .form__input textarea', function (e) {
				let $input = $(this),
					$field = $input.closest('.form__field');

				$field.addClass('f-focused');
			})

			.on('mousewheel', '.form__input textarea', function (e) {
				let $input = $(this);

				$input.blur();
			})

			.on('blur', '.form__input input, .form__input textarea', function (e) {
				let $input = $(this),
					$field = $input.closest('.form__field');
				$field.removeClass('f-focused');

				if ( !$.trim($input.val()) ){
					$field.removeClass('f-filled');
				} else {
					$field.addClass('f-filled');
				}
			})

			.on('mousedown', '.js-how-get-pass', function (e) {
				let $t = $(this),
					$wrap = $t.closest('.form__field');
				tooltipContent.passCheckHold = true;
				tooltip.show($wrap, tooltipContent.passCheckManual);
			})


			.on('keyup focus', '[data-require=password]', function (e) {
				if (!tooltipContent.passCheckHold){
					let $input = $(this), $wrap = $input.closest('.form__field');

					tooltip.show($wrap, tooltipContent.passCheckInfo);

					sheme.validate($input[0], 'password', r =>{

						if (!r.errors.length) {
							sheme.score($input.val(), q =>{
								if ( q.score < 40  ){
									tooltip.show($wrap, tooltipContent.passCheckFalse);
								} else if ( q.score < 60 ){
									tooltip.show($wrap, tooltipContent.passCheckWeak);
								} else if ( q.score < 80 ){
									tooltip.show($wrap, tooltipContent.passCheckModarete);
								} else {
									tooltip.show($wrap, tooltipContent.passCheckStrong);
								}
							});
						}
					});
				}
			})



			.on('blur', '[data-require=password]', function (e) {
				if (!tooltipContent.passCheckHold) tooltip.hide();
			})

			.on('click', '.tooltip__close', function (e) {
				tooltipContent.passCheckHold = false;
				tooltip.hide($(this).closest('.tooltip'));
			})

			.on('click', '.js-close-popup-info', function (e) {
				$(this).closest('.popup__info').removeClass('is-show');
			})

			.on('keyup', 'input', function (e) {
				let $t = $(this),
					$pls = $t.closest('.form__input').find('.placeholder');

				if ( $pls.length && $t.val().length >= 25 ){
					$pls.addClass('is-hidden')
				} else if ( $pls.length && $t.val().length < 25 ){
					$pls.removeClass('is-hidden')
				}
			})




			/////

			.on('submit', 'form', function (e) {
				tooltipContent.passCheckHold = false;
				let $t = $(this);
				tooltip.hide();

				that.check($t, true, (arr)=>{

				}, (arr)=>{
					for(let i = 0; i< arr.length; i++){
						if ( $(arr[i].el).attr('id') === 'checkboxInput2' ){
							tooltip.show($(arr[i].el).closest('.form__field'), tooltipContent.regConf);
						}
					}
					return false;
				});

			})

		//*****


		;



	}
}