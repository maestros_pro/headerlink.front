
export default {

	init(){

		let $b = $('body');

		$b
			.on('change', 'input[type=checkbox]', function (e) {

				let $t = $(this)[0],
					onCheck = $t.dataset.checkboxCheck,
					onUnCheck = $t.dataset.checkboxUncheck
				;

				if ( $t.checked && onCheck ){
					eval(onCheck);
				} else if ( onUnCheck ){
					eval(onUnCheck);
				}

			})
		;
	}
}