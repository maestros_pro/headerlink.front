import axios from 'axios'
//import $ from 'jquery'
import plugin from '../modules/mod.plugins-load'
import Popup from './module.popup'

let popup = new Popup({
	bodyClass: 'is-blur',
	onPopupOpen:()=>{
		plugin.init();
	}
});

export default {

	maxItem: 'Infinity',

	htmlTitle(l){
		return `<div class="popup__category-title">${l}</div>`;
	},

	htmlLetter(l){
		return `<div class="popup__category-letter">${l}</div>`;
	},

	htmlItem(obj, c){
		let dataCategory = c === true ? '' : (obj.categoryGroup || c),
			count = c === true ? '<span class="count"></span>' : '';
		return `<div class="popup__category-link" data-id="${obj.categoryId}" data-name="${obj.name}" data-category="${dataCategory}"><span>${obj.categoryName}</span>${count}<i class="act"></i></div>`;
	},

	addCollection($wrap, obj, cat){

		let that = this,
			$list = $wrap.find('.js-popup-category-result'),
			$inner = $list.find('.popup__category-inner')
		;

		if ( $inner.find('.popup__category-link').length < that.maxItem ){
			$inner.append(that.htmlItem(obj, cat));
			this.checkCollection($wrap);
		}

	},

	removeCollection($wrap, id){
		let $list = $wrap.find('.js-popup-category-result');

		$list.find('[data-id=' + id + ']').closest('.popup__category-link').remove();
	},

	checkCollection($wrap){

		let $listCategory = $wrap.find('.js-popup-category-list'),
			$itemCategory = $listCategory.find('.popup__category-link'),
			$listCollect =  $wrap.find('.js-popup-category-collect'),
			$itemCollect = $listCollect.find('.popup__category-link'),
			$wrapResult =  $wrap.find('.popup__category-result'),
			$listResult =  $wrap.find('.js-popup-category-result'),
			$itemResult = $listResult.find('.popup__category-link'),
			collect = {},
			count = 0,
			$countText = $wrap.find('.js-popup-category-count')
		;

		$itemCategory.removeClass('is-added').find('.count').html('');
		$itemCollect.removeClass('is-added');

		$itemResult.each(function () {
			let $t = $(this),
				id = $t.attr('data-id'),
				cat = $t.attr('data-category')
			;
			if ( !collect[cat] ) collect[cat] = [];
			collect[cat].push(id);

			count++;

			$.each(collect, (key, value)=>{
				//-console.info(key, value);

				if (value.length > 0) {
					$itemCategory.filter('[data-id='+ key +']').addClass('is-added').find('.count').html(value.length);
					// count += value.length
				}

				for (let i = 0; i < value.length; i++ ){
					$itemCollect.filter('[data-category='+ key +']').filter('[data-id='+ value[i] +']').addClass('is-added');
				}
			});

		});


		if ( count > 0 ){
			$wrapResult.removeClass('is-empty');
			$countText.html(count)
		} else {
			$wrapResult.addClass('is-empty');
			$countText.html('')
		}

	},

	setResult(obj, cat){
		let letter = '', html = '', arr = obj.categoryList;

		if ( arr.length ){
			html += '<div class="popup__category-group">';

			for (let i = 0; i < arr.length; i++){

				if ( arr[i].title ){

					if ( obj.initialLetter === 'title' && letter !== arr[i].title[0].toUpperCase() ){
						html += '</div><div class="popup__category-group">';
						letter = arr[i].title[0].toUpperCase();
						html += this.htmlLetter(letter);
					}

					html += this.htmlTitle(arr[i].title);

				} else {

					if ( obj.initialLetter === 'category' && letter !== arr[i].categoryName[0].toUpperCase() ){
						html += '</div><div class="popup__category-group">';
						letter = arr[i].categoryName[0].toUpperCase();
						html += this.htmlLetter(letter);
					}

					html += this.htmlItem(arr[i], cat);

				}
			}

			html += '</div>';

		}

		return html;
	},

	init(){

		let $b = $('body'), that = this, searchTypeRunner = null;

		this.isLoaded = false;

		$b
			.on('click', '.js-popup-category-list .popup__category-link', function (e) {
				e.stopPropagation();

				let $t = $(this),
					$list = $t.closest('.popup__category-list'),
					$wrap = $list.closest('.popup'),
					$collect = $wrap.find('.js-popup-category-collect'),
					$result = $collect.find('.popup__category-inner'),
					url = $list.attr('data-url'),
					id = $t.attr('data-id'),
					data = {}
				;

				data.id = id;

				if ( $wrap.find('.js-popup-category-search').val() ){
					data.value = $wrap.find('.js-popup-category-search').val();
				}

				$t.closest('.popup__category-inner').find('.popup__category-link').removeClass('is-active');
				$t.addClass('is-active');

				if ( url ){
					if (that.isLoaded) return false;
					that.isLoaded = true;
					$collect.addClass('is-loading');

					axios
						.get(url, {
							params: data
						})
						.then(res=>{
							$result.html(that.setResult(res.data, id));
							that.isLoaded = false;
							$collect.removeClass('is-loading');
							that.checkCollection($wrap);
							$collect.mCustomScrollbar('scrollTo', 'top', {timeout: 0, scrollInertia: 0})
						})
						.catch(error => {
							console.error(error);
							that.isLoaded = false;
							$collect.removeClass('is-loading');
						});
				}
			})
			.on('click', '.js-popup-category-list .popup__category-link .act', function (e) {
				e.stopPropagation();
				let $wrap = $(this).closest('.popup'),
					$collection = $wrap.find('.js-popup-category-result'),
					cat = $(this).closest('.popup__category-link').attr('data-id')
				;
				$collection.find('[data-category=' + cat + ']').remove();
				that.checkCollection($(this).closest('.popup'));
			})

			.on('click', '.js-popup-category-collect .popup__category-link .act, .js-popup-category-collect .popup__category-link span', function (e) {
				e.stopPropagation();
				let $link = $(this).closest('.popup__category-link'),
					isHandler = $(this).hasClass('act'),
					isAdded = $link.hasClass('is-added'),
					id = $link.attr('data-id'),
					name = $link.attr('data-name'),
					categoryName = $link.find('span').html(),
					cat = $link.attr('data-category')
				;

				if ( isAdded && isHandler ){
					$link.removeClass('is-added');
					that.removeCollection($(this).closest('.popup'), id);
				} else if ( !isAdded ) {
					$link.addClass('is-added');
					that.addCollection($(this).closest('.popup'), {categoryId:id, categoryName, name}, cat);
				}

				that.checkCollection($(this).closest('.popup'));
			})

			.on('click', '.js-popup-category-result .popup__category-link .act', function (e) {
				e.stopPropagation();
				let $wrap = $(this).closest('.popup');
				$(this).closest('.popup__category-link').remove();
				that.checkCollection($wrap);
			})

			.on('click', '.popup__category-result .js-clear-result', function (e) {
				e.stopPropagation();
				let $wrap = $(this).closest('.popup');
				$(this).closest('.popup__category-result').find('.popup__category-inner').html('');
				that.checkCollection($wrap);
			})

			.on('click', '[data-add-category-popup]', function (e) {
				e.stopPropagation();
				let $t = $(this),
					popupCat = $t.attr('data-add-category-popup'),
					container = $t.attr('data-add-category-container'),
					$popup = popup.open(popupCat),
					$container = $(container)
				;

				that.maxItem = $t.attr('data-add-category-max') || 'Infinity';

				$('[data-container-wrap]').removeAttr('data-container-wrap');

				if ( !!$t.closest('.popup').length ){
					$t.closest('.popup').attr('data-container-wrap', container);
				} else {
					$container.attr('data-container-wrap', container);
				}

				$($popup).find('.js-popup-category-result').find('.popup__category-inner').html('');
				that.checkCollection($($popup));

				$container.find('.form__categories-item').each(function () {
					let $t = $(this),
						categoryName = $t.find('.form__categories-val').html(),
						name = $t.find('input').attr('name'),
						input = $t.find('input').attr('data-category-input').match(/\[(\w+)]/g),
						categoryId = input[1].replace('[', '').replace(']', ''),
						categoryGroup = input[0].replace('[', '').replace(']', '')
					;
					let data = {
						categoryId,
						categoryName,
						categoryGroup,
						name
					};

					that.addCollection($($popup), data)
				});

			})


			.on('click', '.js-category-clear', function (e) {
				e.stopPropagation();
				$(this).closest('.form__categories-item').remove();
			})

			.on('click', '.js-popup-categories-confirm', function (e) {
				e.stopPropagation();
				let $t = $(this),
					$wrap = $t.closest('.popup'),
					$target = $('[data-container-wrap]'),
					container = $target.attr('data-container-wrap'),
					html = ''
				;

				$wrap.find('.popup__category-result').find('.popup__category-link').each(function () {
					let $t = $(this),
						id = $t.attr('data-id'),
						name = $t.attr('data-name'),
						cat = $t.attr('data-category'),
						val = $t.find('span').html()
					;

					html += '<div class="form__categories-item">' +
								'<div class="form__categories-val">' + val + '</div>' +
								'<div class="form__categories-clear js-category-clear"></div>' +
								'<input type="hidden" data-category-input="[' + cat + '][' + id + ']" name="' + name + '" value="' + id + '">' +
							'</div>';
				});

				if ($target.hasClass('popup')) {
					let popupBack = popup.open($target[0]);
					$target.find(container).html(html);
					$target.removeAttr('data-container-wrap');
				} else {
					popup.close();
					$target.html(html);
				}

			})

			.on('mousedown', '.js-popup-categories-cancel', function (e) {
				e.stopPropagation();
				let $target = $('[data-container-wrap]');

				if ($target.hasClass('popup')) {
					let popupBack = popup.open($target[0]);
				} else {
					popup.close();
				}
				$target.removeAttr('data-container-wrap');

			})

			// local category search
			/*.on('keyup', '.js-popup-category-search', function (e) {

				let $t = $(this),
				val = $.trim($t.val()).toLowerCase().replace('ё', 'е'),
				$container = $t.closest('.popup').find('.js-popup-category-list'),
				$collection = $t.closest('.popup').find('.js-popup-category-collect'),
				$group = $container.find('.popup__category-group'),
				$item = $group.find('.popup__category-link'),
				$messageMain = $container.find('[data-text-main]'),
				messageMain = $messageMain.attr('data-text-main'),
				messageNofFound = $messageMain.attr('data-text-notfound')
				;

				$group.removeClass('is-found');
				$item.removeClass('is-found');

				if (val.length){
					$container.addClass('is-scan');
					$item.each(function () {
						let $t = $(this), text = $t.find('span').eq(0).text().toLowerCase().replace('ё', 'е');

						if ( text.indexOf(val) >= 0 ){
							$t.addClass('is-found').closest('.popup__category-group').addClass('is-found');
						}
					});

					if ( $container.find('.is-found').length < 1 ){
						$messageMain.html(messageNofFound);
					} else {
						$messageMain.html(messageMain);
					}

					if ( !$container.find('.is-active').hasClass('is-found') ){
						$collection.find('.popup__category-inner').html('');
					}
				} else {
					$container.removeClass('is-scan');
					$messageMain.html(messageMain);
				}

			})*/

			.on('keyup', '#regInputActualAddress, #regInputLegalAddress', function (e) {
				$('#regCheckboxJoinAddress').prop('checked', false);
			})

			// server category search
			.on('keyup', '.js-popup-category-search', function (e) {

				let $t = $(this),
					url = $t.attr('data-action'),
					val = $t.val(),
					$wrap = $t.closest('.popup'),
					$container = $wrap.find('.js-popup-category-list'),
					$result = $container.find('.popup__category-inner'),

					$messageMain = $container.find('[data-text-main]'),
					messageMain = $messageMain.attr('data-text-main'),
					messageNofFound = $messageMain.attr('data-text-notfound')
				;


				$container.addClass('is-loading');

				clearTimeout(searchTypeRunner);
				searchTypeRunner = setTimeout(()=>{

					axios
						.get(url, {
							params: {
								value: val
							}
						})
						.then(res=>{

							if (res.data && res.data.categoryList.length < 1 ){
								$messageMain.html(messageNofFound);
							} else {
								$messageMain.html(messageMain);
								$result.html(that.setResult(res.data, true));
								that.isLoaded = false;
								$container.removeClass('is-loading');

								$result.find('.popup__category-link').eq(0).click();

							}
						})
						.catch(error => {
							console.error(error);
							that.isLoaded = false;
							$container.removeClass('is-loading');
						});
				}, 300);

			})
		;
	}
}