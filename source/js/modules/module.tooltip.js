import $ from 'jquery';
'use strict';


export default class Tooltip {
	constructor(){

	}

	show(element, options){

		this.element = element;

		let $tt = $(element).children('.tooltip'),
			html = '',
			opts = options || {},
			direction = opts.direction ? ('tooltip_' + opts.direction) : '';

		if ( !$tt.length ){
			$tt =	$('<div class="tooltip ' + direction +'">'
				+		'<div class="tooltip__inner"></div>'
				+	'</div>').appendTo(element);
		}

		if ( $tt.is(':hidden') ) $tt.fadeIn();

		this.$inner = $tt.find('.tooltip__inner').removeAttr('style').html('');

		if ( opts.close )	html += '<div class="tooltip__close"></div>';
		if ( opts.title )	html += '<div class="tooltip__title">'+opts.title+'</div>';
		if ( opts.subtitle )	html += '<div class="tooltip__subtitle">'+opts.subtitle+'</div>';
		if ( opts.html ) 	html += opts.html;

		if ( html ) this.$inner.html(html);
		this._setPosition();

	}

	_setPosition(){
		let o = window.innerHeight - (this.$inner.parent().offset().top + this.$inner.outerHeight() - $(window).scrollTop());
		if ( o < 0 ) this.$inner.css({'margin-top': o});
	}


	hide(element){
		let $tt = element ? $(element) : $('.tooltip');

		$tt.fadeOut(function () {
			$(this).remove();
		});
	}

}