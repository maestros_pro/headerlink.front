import plugin from '../modules/mod.plugins-load'
import Popup from '../modules/module.popup'
import Cropper from 'cropperjs'

let popup = new Popup({
	bodyClass: 'is-blur',
	onPopupOpen:()=>{
		plugin.init();
	},
	onPopupClose:(p, e)=>{

	}
});


export default {

	_src: null,

	_currentSrc: null,

	_currentData: {},

	_cropper: null,

	_initCropper($popup, s){

		if ( this._cropper ) this._cropper.destroy();

		let that = this, src = s || that._src;
		$popup.find('.popup__content-inner').hide();
		$popup.find('.js-view-afterload').show();


		//console.log('%c                     ', 'font-size:100px;line-height:100px;display:block;background:url(' + src + ');width:100px;height:100px;background-size:cover;');


		let $container = $popup.find('.popup__load-cropper-container');
		$container.html('');

		let $img = $('<img/>', {src: src}).appendTo($container);

		let cropper;

		cropper = new Cropper($img[0], {
			aspectRatio: 1,
			guides: false,
			zoomable: false,
			background: false,
			center: false,
			viewMode: 0,
			toggleDragModeOnDblclick: false,
			crop: (e)=>{

			},
			cropend(e){
				/*let img = this._cropper.getCanvasData(),
					box = this._cropper.getCropBoxData(),
					fix = checkCroppBox(img, box)
				;

				if (fix.fixed){
					this._cropper.setCropBoxData(fix.box)
				}*/
			}
		});

		this._cropper = cropper;

		if (s){
			this._cropper.setData(this._currentData);
		}


		function checkCroppBox(img, box) {
			let offset = 10, fixed = false;
			if ( img.top - box.top < 10 ) {
				box.top = img.top;
				fixed = true;
			}
			if ( img.left - box.left < 10 ) {
				box.left = img.left;
				fixed = true;
			}
			if ( box.top + box.height - img.top + img.height < 10 ) {
				box.top = img.top;
				fixed = true;
			}
			if ( box.left + box.width - img.left + img.width < 10 ) {
				box.left = img.left;
				fixed = true;
			}

			return {fixed, box}
		}

	},

	_loadAvatar(files, $popup){

		let file = files[0],
			$barPie = $popup.find('.svg-pie'),
			$barPercent = $popup.find('.popup__load-progress-val'),
			progressLoaded = 0,
			isLoadedPie = false,
			isLoadedReader = false,
			reader,
			progressTimer = null;

		$popup.find('input').val('');

		if ( file && !/\.(jpg|jpeg|png|gif)$/i.test( file.name ) ) {
			alert('не верный тип файла');
		} else if ( file.size > 8000000 ) {
			alert('слишком большой размер файла');
		} else if (this && window.File && window.FileReader && window.FileList && window.Blob) {

			$popup.find('.popup__content-inner').hide();
			$popup.find('.js-view-load').show();

			reader = new FileReader();
			reader.onprogress = (e)=>{

				if (e.lengthComputable) {

					let pr = Math.ceil(100 / e.total * e.loaded);

					progressTimer = setInterval(()=>{
						let loaded = pr < progressLoaded ? pr : progressLoaded;
						$barPie.css({'stroke-dashoffset': 500 - ((500*loaded/100))});
						$barPercent.html(loaded + '%');
						progressLoaded++;

						if (loaded >= 100) {
							if ( isLoadedReader ) this._initCropper($popup);
							clearInterval(progressTimer);
							isLoadedPie = true;
						}
					}, 10);

				}
			};
			reader.onload = ()=>{
				this._src = reader.result;
				if ( isLoadedPie ) this._initCropper($popup);
				isLoadedReader = true;
				$popup.find('.js-view-afterload').find('.popup__load-cropper-container img').attr('src', this._src);
			};

			reader.readAsDataURL(file);
		}

	},


	init(){

		let that = this,
			dashoffset = 500,
			$popupAvatarLoad = $('#popupImageLoad'),
			$image = $('#avatarImageCropp')
		;


		let $b = $('body');

		// drop n down file upload
		$b

			.on('click', '[data-popup-avatar]', function (e) {
				e.stopPropagation();
				e.preventDefault();

				let $t = $(this),
					type = $t.attr('data-popup-avatar')
				;

				if ( type === 'add' || type === 'edit' ){

					if ( $t.closest('.popup').length ) $t.closest('.popup').attr('data-popup-reopen', 'true');

					$('[data-container-wrap]').removeAttr('data-container-wrap');
					$t.closest('.form__field').attr('data-container-wrap', 'true');
					popup.open($popupAvatarLoad[0]);
				}

				if ( type === 'add' || type === 'stop' ){
					$popupAvatarLoad.find('.popup__content-inner')
						.hide()
						.filter('.js-view-preload').show();
				}

				if ( type === 'edit' ){
					$popupAvatarLoad
						.find('.popup__content-inner')
						.hide()
						.filter('.js-view-afterload').show();

					if ( that._currentData ) {
						that._initCropper($popupAvatarLoad, that._currentSrc);
						that._src = that._currentSrc;
					}

				}

				if ( type === 'cancel' ){
					if ( $('[data-popup-reopen]').length ){
						popup.open($('[data-popup-reopen]')[0]);
						$('[data-popup-reopen]').removeAttr('data-popup-reopen');
					} else {
						popup.close();
					}
				}


				if ( type === 'clear' ){
					$image.removeAttr('style').removeClass('is-edited is-loaded').find('input').val(null);
					that._currentSrc = null;
					that._currentData = null;
				}



				if ( type === 'save' ){

					let $container = $('[data-container-wrap]'),
						img = that._cropper.getCroppedCanvas({
						fillColor: '#fff',
						width: 400,
						height: 400,
						imageSmoothingEnabled: false,
						imageSmoothingQuality: 'high'
					});

					that._currentSrc = that._src;
					that._currentData = that._cropper.getData();


					popup.close($popupAvatarLoad[0]);
					if ( $container.closest('.popup').length ) popup.open($container.closest('.popup')[0]);
					$container
						.removeAttr('data-container-wrap')
						.find('.image')
						.addClass('is-edited is-loaded')
						.css({'background-image': 'url('+img.toDataURL()+')'})
						.find('input').val(img.toDataURL());
				}

			})

			.on('dragenter', '.popup_imageload', function (e) {
				e.stopPropagation();
				e.preventDefault();
				let $t = $(this);

				if (!$t.hasClass('p-dragged')) $t.addClass('p-dragged');
			})

			.on('dragover', '.popup_imageload', function (e) {
				e.stopPropagation();
				e.preventDefault();
				let $t = $(this);

				if (!$t.hasClass('p-dragged')) $t.addClass('p-dragged');

			})

			.on('dragleave', '.popup_imageload .popup__dragbox', function (e) {
				e.stopPropagation();
				e.preventDefault();
				let $t = $(this);

				$t.closest('.popup').removeClass('p-dragged');

			})

			.on('drop', '.popup_imageload .popup__dragbox', function (e) {
				e.stopPropagation();
				e.preventDefault();
				let $t = $(this);

				$t.closest('.popup').removeClass('p-dragged');

				let dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer,
					files = dataTransfer.files;

				if (files.length > 0) that._loadAvatar(files, $t.closest('.popup'));

			})

			.on('change', '#popupFileLoadInput', function (e) {
				if (e.target.files.length > 0) that._loadAvatar(e.target.files, $(e.target.closest('.popup')));
			})

			.on('click', '#popupImageLoad .control-btn_rotatecw', function (e) {
				that._cropper.rotate(90);
			})

			.on('click', '#popupImageLoad .control-btn_rotateccw', function (e) {
				that._cropper.rotate(-90);
			})


			.on('click', '.control-btn_clear', function (e) {
				that._cropper.destroy();
				$popupAvatarLoad
					.find('.popup__content-inner')
					.hide()
					.filter('.js-view-preload').show();
			})

			.on('click', '.popup__close', function (e) {
				if ( $('[data-popup-reopen]').length ){
					popup.open($('[data-popup-reopen]')[0]);
					$('[data-popup-reopen]').removeAttr('data-popup-reopen');
				}
			})

		;



	}
}