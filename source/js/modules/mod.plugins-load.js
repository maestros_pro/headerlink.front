export default {

	select(){
		let that = this;

		$('select').each(function () {
			let $t = $(this);

			if (!$t.data('inited')) {
				$t.data('inited', true);
				$t.select2({
					mouseWheel: true,
					width: false,
					language: {
						noResults: function () {
							return 'Поиск не дал результатов';
						}
					},
					templateResult: (state)=>{
						console.info(state.element);

						let html,
							dataWrapTag = $(state.element).attr('data-wrap-tag'),
							dataIco = $(state.element).attr('data-icon'),
							img = dataIco ? '<img class="select2-icon" src="' + dataIco + '">' : '',
							wrapTagStart = dataWrapTag ? '<'+dataWrapTag+'>' : '',
							wrapTagEnd = dataWrapTag ? '</'+dataWrapTag+'>' : ''
						;

						html = '<div>' + wrapTagStart + img + state.text +  wrapTagEnd + '</div>';
						return $(html);
					}
				}).on('select2:open', (e)=>{
					$(".select2-results ul.select2-results__options").unbind("mousewheel");
					that.scroll();
				});
			}
		});
	},

	mask(){
		$('[data-inputmask-mask]').each(function () {
			let $t = $(this);

			if (!$t.data('inited')) {
				$t.data('inited', true);
				$t.inputmask({
					mask: $t.attr('data-inputmask-mask'),
					onincomplete: function () {
						this.value = '';
					}
				});
			}

		});
	},

	getCursorPosition($el){
		let el=$el.get(0),
			pos=0;
		if("selectionStart" in el){
			pos=el.selectionStart;
		}else if("selection" in document){
			el.focus();
			let sel=document.selection.createRange(),
				selLength=document.selection.createRange().text.length;
			sel.moveStart("character",-el.value.length);
			pos=sel.text.length-selLength;
		}
		return pos;
	},

	textareaScrollBar(){

		let that = this;

		$('.form__input_textarea').each(function () {
			let $t = $(this).find('textarea');

			if (!$t.data('inited')) {
				$t.data('inited', true);


				let textArea = $t;
				// textArea.wrap("<div class='textarea-wrapper' />");
				let textAreaWrapper=textArea.closest('.form__input_textarea');
				textAreaWrapper.mCustomScrollbar({
					// scrollInertia:0,
					advanced:{autoScrollOnFocus:false}
				});
				let hiddentextarea=$(document.createElement("div")),
					content=null;
				hiddentextarea.addClass("hiddentextarea");

				$("body").prepend(hiddentextarea);

				textArea.bind("keyup",function(e){
					content=$(this).val();
					let cursorPosition= that.getCursorPosition(textArea);
					content="<span>"+content.substr(0,cursorPosition)+"</span>"+content.substr(cursorPosition,content.length);
					content=content.replace(/\n/g,"<br />");
					hiddentextarea.html(content+"<br />");
					$(this).css("height",hiddentextarea.height());
					textAreaWrapper.mCustomScrollbar("update");
					let hiddentextareaSpan=hiddentextarea.children("span"),
						hiddentextareaSpanOffset=0,
						viewLimitBottom=(parseInt(hiddentextarea.css("min-height")))-hiddentextareaSpanOffset,
						viewRatio=Math.round(hiddentextareaSpan.height()+textAreaWrapper.find(".mCSB_container").position().top);
					if(viewRatio>viewLimitBottom || viewRatio<hiddentextareaSpanOffset){
						if((hiddentextareaSpan.height()-hiddentextareaSpanOffset)>0){
							textAreaWrapper.mCustomScrollbar("scrollTo",hiddentextareaSpan.height()-hiddentextareaSpanOffset);
						}else{
							textAreaWrapper.mCustomScrollbar("scrollTo","top");
						}
					}
				});

			}

		});

	},

	scroll(){
		$('.js-scrollbar').each(function () {
			let $t = $(this);

			if (!$t.data('inited') && $t.is(':visible')) {
				$t.data('inited', true);
				$t.mCustomScrollbar();
			}

		});
	},

	init(){

		setTimeout(()=>{
			this.mask();
			this.select();
			this.scroll();
			this.textareaScrollBar();
		}, 10);

	}
}